package Pieces;


import board.Board;

public class Ibex extends Piece {

    // Constructor
    public Ibex(int x, int y, boolean isWhite, String filePath, Board board) {
        // I use the constructor of the super class
        super(x, y, isWhite, filePath, board);
    }

    @Override
    public boolean canMove(int destinationX, int destinationY) {
        // Gets the possible piece where the player intends to move
        Piece possiblePiece = board.getPiece(destinationX, destinationY);

        // Situation when the destination piece is the same colour
        if (possiblePiece != null) {    // If there is a piece
            if (possiblePiece.isWhite() && this.isWhite()) {    // True if destination piece is white and player is white
                return false;   // We can not kill our own piece
            }
            if (possiblePiece.isBlack() && this.isBlack()) {    // True if destination piece is black and player is black
                return false;   // We can not kill our own piece
            }
        }

        // Row and Column delta squares
        int rowDelta = Math.abs(destinationX - this.getX());
        int columnDelta = Math.abs(destinationY - this.getY());

        // The ibex can only move if there is a difference of 1 in a row and 2 in a column
        if ((rowDelta == 1) && (columnDelta == 2)) {
            return true;
        }
        // The ibex can only move if there is a difference of 1 in a row and 2 in a column
        if ((columnDelta == 1) && (rowDelta == 2)) {
            return true;
        }

        return false;
    }
}
