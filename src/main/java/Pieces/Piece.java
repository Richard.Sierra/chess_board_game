package Pieces;

import board.Board;

public class Piece {
    // Coordinates
    private int x;
    private int y;

    // Piece color
    final private boolean isWhite;

    // Image file path
    private String filePath;

    // The board where it is used
    public Board board;

    // Constructor
    public Piece(int x, int y, boolean isWhite, String filePath, Board board) {
        this.isWhite = isWhite;
        this.x = x;
        this.y = y;
        this.filePath = filePath;
        this.board = board;
    }

    // Colors boolean functions
    public boolean isWhite() {
        return isWhite;
    }

    public boolean isBlack() {
        return !isWhite;
    }

    // Logic function (implemented differently in each game piece)
    public boolean canMove(int destination_x, int destination_y) {
        return false;
    }

    // Getters and Setters

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String path) {
        this.filePath = path;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
