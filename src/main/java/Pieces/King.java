package Pieces;


import board.Board;

public class King extends Piece {

    // Constructor
    public King(int x, int y, boolean isWhite, String filePath, Board board) {
        // I use the constructor of the super class
        super(x, y, isWhite, filePath, board);
    }

    @Override
    public boolean canMove(int destinationX, int destinationY) {
// Gets the possible piece where the player intends to move
        Piece possiblePiece = board.getPiece(destinationX, destinationY);

        // Situation when the destination piece is the same colour
        if (possiblePiece != null) {    // If there is a piece
            if (possiblePiece.isWhite() && this.isWhite()) {    // True if destination piece is white and player is white
                return false;   // We can not kill our own piece
            }
            if (possiblePiece.isBlack() && this.isBlack()) {    // True if destination piece is black and player is black
                return false;   // We can not kill our own piece
            }
        }

        // Row and Column delta squares
        int rowDelta = Math.abs(destinationX - this.getX());
        int columnDelta = Math.abs(destinationY - this.getY());

        // The King can only move if there is a difference of 1 in a row or 1 in a column
        if ((rowDelta == 1) || (columnDelta == 1)) {
            return true;
        }

        return false;
    }
}
