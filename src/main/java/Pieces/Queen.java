package Pieces;


import board.Board;

public class Queen extends Piece {

    // Constructor
    public Queen(int x, int y, boolean isWhite, String filePath, Board board) {
        // I use the constructor of the super class
        super(x, y, isWhite, filePath, board);
    }

    @Override
    public boolean canMove(int destinationX, int destinationY) {
        // Gets the possible piece where the player intends to move
        Piece possiblePiece = board.getPiece(destinationX, destinationY);

        // Situation when the destination piece is the same colour
        if (possiblePiece != null) {    // If there is a piece
            if (possiblePiece.isWhite() && this.isWhite()) {    // True if destination piece is white and player is white
                return false;   // We can not kill our own piece
            }
            if (possiblePiece.isBlack() && this.isBlack()) {    // True if destination piece is black and player is black
                return false;   // We can not kill our own piece
            }
        }

        // Row and Column delta squares
        int rowDelta = Math.abs(destinationX - this.getX());
        int columnDelta = Math.abs(destinationY - this.getY());

        // Number of squares the bishop wants to move
        int squaresToMove = Math.abs(destinationX - this.getX());

        boolean straightMove = this.getX() != destinationX && this.getY() != destinationY;
        boolean diagonalMove = rowDelta - columnDelta != 0;

        if (straightMove && diagonalMove) {
            return false;
        }


        // Determine the direction and the number of squares the rook wants to move
        String direction = "";

        if (destinationY > this.getY() && destinationX > this.getX()) {
            direction = "DownRight";
        }
        if (destinationY > this.getY() && destinationX < this.getX()) {
            direction = "DownLeft";
        }
        if (destinationY < this.getY() && destinationX > this.getX()) {
            direction = "UpRight";
        }
        if (destinationY < this.getY() && destinationX < this.getX()) {
            direction = "UpLeft";
        }
        if (destinationY > this.getY()) {
            direction = "Down";
            squaresToMove = Math.abs(destinationY - this.getY());  // Vertical squares
        }
        if (destinationY < this.getY()) {
            direction = "Up";
            squaresToMove = Math.abs(destinationY - this.getY());  // Vertical squares
        }
        if (destinationX > this.getX()) {
            direction = "Right";
            squaresToMove = Math.abs(destinationX - this.getX());  // Horizontal squares
        }
        if (destinationX < this.getX()) {
            direction = "Left";
            squaresToMove = Math.abs(destinationX - this.getX());  // Horizontal squares
        }

        // The queen can not jump over other pieces
        switch (direction) {
            case "DownRight":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX() + i, this.getY() + i);
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "DownLeft":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX() - i, this.getY() + i);
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "UpRight":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX() + i, this.getY() - i);
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "UpLeft":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX() - i, this.getY() - i);
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "Down":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX(), this.getY() + i);
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "Up":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX(), this.getY() - i);
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "Right":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX() + i, this.getY());
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "Left":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX() - i, this.getY());
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            default:
                return false;
        }
        return true;
    }
}
