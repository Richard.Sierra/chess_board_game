package Pieces;


import board.Board;

public class Pawn extends Piece {

    private boolean hasMoved;

    // Constructor
    public Pawn(int x, int y, boolean isWhite, String filePath, Board board) {
        // I use the constructor of the super class
        super(x, y, isWhite, filePath, board);
        hasMoved = false;
    }

    // Getters nd Setters
    public void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    public boolean getHasMoved() {
        return hasMoved;
    }

    @Override
    public boolean canMove(int destinationX, int destinationY) {
        // Gets the possible piece where the player intends to move
        Piece possiblePiece = board.getPiece(destinationX, destinationY);

        // Situation when the destination piece is the same colour
        if (possiblePiece != null) {    // If there is a piece
            if (possiblePiece.isWhite() && this.isWhite()) {    // True if destination piece is white and player is white
                return false;   // We can not kill our own piece
            }
            if (possiblePiece.isBlack() && this.isBlack()) {    // True if destination piece is black and player is black
                return false;   // We can not kill our own piece
            }
        }

        // The pawn can move max 2 squares if it never moved otherwise 1 square
        int maxMove = 2;
        if (this.hasMoved) {
            maxMove = 1;
        }

        // Number of squares the pawn wants to move
        int squaresToMove = Math.abs(destinationY - this.getY());  // Vertical squares

        // Check if it respect the max limit
        if (squaresToMove > maxMove) {
            return false;
        }


        boolean up = destinationY < this.getY();
        boolean down = destinationY > this.getY();
        boolean sameColumn = this.getX() == destinationX;

        boolean down1 = destinationY == this.getY() + 1;
        boolean right1 = destinationX == this.getX() + 1;
        boolean up1 = destinationY == this.getY() - 1;
        boolean left1 = destinationX == this.getX() - 1;


        // Determine the direction the pawn wants to move
        String direction = "";

        if (destinationY > this.getY() && destinationX > this.getX()) {
            direction = "DownRight";
        }
        if (destinationY > this.getY() && destinationX < this.getX()) {
            direction = "DownLeft";
        }
        if (destinationY < this.getY() && destinationX > this.getX()) {
            direction = "UpRight";
        }
        if (destinationY < this.getY() && destinationX < this.getX()) {
            direction = "UpLeft";
        }

        // Determine the vertical directions

        if (down) {
            direction = "Down";
        }
        if (up) {
            direction = "Up";
        }

        Piece upRight = board.getPiece(this.getY() - 1, this.getX() + 1);
        Piece upLeft = board.getPiece(this.getY() - 1, this.getX() - 1);
        Piece downRight = board.getPiece(this.getY() + 1, this.getX() + 1);
        Piece downLeft = board.getPiece(this.getY() + 1, this.getX() - 1);

        switch (direction) {
            case "UpRight":
                if (this.isBlack()) {
                    return true;
                }
                break;
            case "UpLeft":
                if (this.isBlack()) {
                    return true;
                }
                break;
            case "DownRight":
                if (this.isWhite()) {
                    return true;
                }
                break;
            case "DownLeft":
                if (this.isWhite()) {
                    return true;
                }
                break;
            case "Up":
                // Check if it respects the same column
                if (!sameColumn) {
                    return false;
                }
                if (this.isBlack()) {
                    // Iterate through each square until the destination
                    for (int i = 1; i <= squaresToMove; i++) {
                        // Gets the piece at coordinates
                        Piece piece = board.getPiece(this.getX(), this.getY() - i);
                        if (piece != null) {   // True if there is a piece
                            return false;      // Don't allow movement because there is a piece between
                        }
                    }
                } else {
                    return false;
                }
                break;
            case "Down":
                // Check if it respects the same column
                if (!sameColumn) {
                    return false;
                }
                if (this.isWhite()) {
                    // Iterate through each square until the destination
                    for (int i = 1; i <= squaresToMove; i++) {
                        // Gets the piece at coordinates
                        Piece piece = board.getPiece(this.getX(), this.getY() + i);
                        if (piece != null) {   // True if there is a piece
                            return false;      // Don't allow movement because there is a piece between
                        }
                    }
                } else {
                    return false;
                }
                break;
            default:
                return false;
        }
        return true;
    }
}
