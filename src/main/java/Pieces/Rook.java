package Pieces;


import board.Board;

public class Rook extends Piece {

    // Constructor
    public Rook(int x, int y, boolean isWhite, String filePath, Board board) {
        // I use the constructor of the super class
        super(x, y, isWhite, filePath, board);
    }

    @Override
    public boolean canMove(int destinationX, int destinationY) {
        // Gets the possible piece where the player intends to move
        Piece possiblePiece = board.getPiece(destinationX, destinationY);

        // Situation when the destination piece is the same colour
        if (possiblePiece != null) {    // If there is a piece
            if (possiblePiece.isWhite() && this.isWhite()) {    // True if destination piece is white and player is white
                return false;   // We can not kill our own piece
            }
            if (possiblePiece.isBlack() && this.isBlack()) {    // True if destination piece is black and player is black
                return false;   // We can not kill our own piece
            }
        }

        // The rook can only move in a straight line
        if (this.getX() != destinationX && this.getY() != destinationY) {
            return false;   // The idea is to match one of the x or y destination to the current position
        }

        // Determine the direction and the number of squares the rook wants to move
        String direction = "";
        int squaresToMove = 0;

        if (destinationY > this.getY()) {
            direction = "Down";
            squaresToMove = Math.abs(destinationY - this.getY());  // Vertical squares
        }
        if (destinationY < this.getY()) {
            direction = "Up";
            squaresToMove = Math.abs(destinationY - this.getY());  // Vertical squares
        }
        if (destinationX > this.getX()) {
            direction = "Right";
            squaresToMove = Math.abs(destinationX - this.getX());  // Horizontal squares
        }
        if (destinationX < this.getX()) {
            direction = "Left";
            squaresToMove = Math.abs(destinationX - this.getX());  // Horizontal squares
        }

        // The rook can not jump over other pieces
        switch (direction) {
            case "Down":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX(), this.getY() + i);
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "Up":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX(), this.getY() - i);
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "Right":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX() + i, this.getY());
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
            case "Left":
                // Iterate through each square until the destination
                for (int i = 1; i < squaresToMove; i++) {
                    // Gets the piece at coordinates
                    Piece piece = board.getPiece(this.getX() - i, this.getY());
                    if (piece != null) {   // True if there is a piece
                        return false;      // Don't allow movement because there is a piece between
                    }
                }
                break;
        }

        return true;
    }
}
