package board;

import Pieces.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Board extends JComponent {

    // With a modulo operation we can determine whether is the whites turn or not
    public int turnCounter = 0;

    // Will show an amazing Ibex if the program cannot read an image file
    private final Image NULL_IMAGE = loadImage("src/main/resources/images/IbexIcon.png");

    // Defines the size of each square
    private final int squareWidth = 112;

    // The white and black pieces will be stored in an ArrayList of type <Piece>
    public ArrayList<Piece> whitePieces;
    public ArrayList<Piece> blackPieces;

    // Will be used with the Graphics2d class from java.awt to render the images
    public ArrayList<DrawingShape> staticShapesGraphics;
    public ArrayList<DrawingShape> piecesGraphics;

    // This is the active piece after clicking
    public Piece activePiece;

    // Defines the number of rows and columns in a chess board  (8X8)
    private final int rows = 8;
    private final int columns = 8;

    // The board grid is an array of array
    private final Integer[][] BoardGrid;


    // Constructor :creates a board for the game
    public Board() {

        // Initialise the boardGrid and all the others ArrayList
        BoardGrid = new Integer[rows][columns];
        staticShapesGraphics = new ArrayList();
        piecesGraphics = new ArrayList();
        whitePieces = new ArrayList();
        blackPieces = new ArrayList();

        // Initialise a grid with the pieces at start of the game
        initGrid();

        // JComponent settings :
        this.setBackground(new Color(26, 26, 28));
        this.setPreferredSize(new Dimension(896, 896));
        this.setMinimumSize(new Dimension(112, 112));
        this.setMaximumSize(new Dimension(896, 896));
        this.addMouseListener(mouseAdapter);
        this.setVisible(true);
        drawBoard();
    }

    // Initialise a grid with the pieces at start of the game
    public void initGrid() {
        // Will initialise the boardGrid with a temporary value of 0
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < columns; y++) {
                BoardGrid[x][y] = 0;
            }
        }

        // Add a new instance of a piece into the Array List of white pieces
        whitePieces.add(new King(3, 0, true, "King.png", this));
        whitePieces.add(new Queen(4, 0, true, "Queen.png", this));
        whitePieces.add(new Bishop(2, 0, true, "Bishop.png", this));
        whitePieces.add(new Bishop(5, 0, true, "Bishop.png", this));
        whitePieces.add(new Ibex(1, 0, true, "Ibex.png", this));
        whitePieces.add(new Ibex(6, 0, true, "Ibex.png", this));
        whitePieces.add(new Rook(0, 0, true, "Rook.png", this));
        whitePieces.add(new Rook(7, 0, true, "Rook.png", this));
        whitePieces.add(new Pawn(0, 1, true, "Pawn.png", this));
        whitePieces.add(new Pawn(1, 1, true, "Pawn.png", this));
        whitePieces.add(new Pawn(2, 1, true, "Pawn.png", this));
        whitePieces.add(new Pawn(3, 1, true, "Pawn.png", this));
        whitePieces.add(new Pawn(4, 1, true, "Pawn.png", this));
        whitePieces.add(new Pawn(5, 1, true, "Pawn.png", this));
        whitePieces.add(new Pawn(6, 1, true, "Pawn.png", this));
        whitePieces.add(new Pawn(7, 1, true, "Pawn.png", this));

        // Add a new instance of a piece into the Array List of black pieces
        blackPieces.add(new King(3, 7, false, "King.png", this));
        blackPieces.add(new Queen(4, 7, false, "Queen.png", this));
        blackPieces.add(new Bishop(2, 7, false, "Bishop.png", this));
        blackPieces.add(new Bishop(5, 7, false, "Bishop.png", this));
        blackPieces.add(new Ibex(1, 7, false, "Ibex.png", this));
        blackPieces.add(new Ibex(6, 7, false, "Ibex.png", this));
        blackPieces.add(new Rook(0, 7, false, "Rook.png", this));
        blackPieces.add(new Rook(7, 7, false, "Rook.png", this));
        blackPieces.add(new Pawn(0, 6, false, "Pawn.png", this));
        blackPieces.add(new Pawn(1, 6, false, "Pawn.png", this));
        blackPieces.add(new Pawn(2, 6, false, "Pawn.png", this));
        blackPieces.add(new Pawn(3, 6, false, "Pawn.png", this));
        blackPieces.add(new Pawn(4, 6, false, "Pawn.png", this));
        blackPieces.add(new Pawn(5, 6, false, "Pawn.png", this));
        blackPieces.add(new Pawn(6, 6, false, "Pawn.png", this));
        blackPieces.add(new Pawn(7, 6, false, "Pawn.png", this));
    }


    // Will "draw" the board applying Graphics2d class
    private void drawBoard() {
        // Clears the content of the two arrays lists
        piecesGraphics.clear();
        staticShapesGraphics.clear();
        // Defines the board and the active square image path
        String boardFilePath = "src/main/resources/images/board.png";
        String activeSquareFilePath = "src/main/resources/images/active_square.png";

        // Instantiate the board image
        Image board = loadImage(boardFilePath);
        // Add to the staticShapesGraphics array list an instance of the DrawingImage class for the shape of the board
        staticShapesGraphics.add(
                new DrawingImage(
                        board,
                        new Rectangle2D.Double(
                                0,
                                0,
                                board.getWidth(null),
                                board.getHeight(null)
                        )
                )
        );
        // Only if there is a piece in the clicked square
        if (activePiece != null) {
            // Instantiate the active square image
            Image activeSquare = loadImage(activeSquareFilePath);
            // Add to the staticShapesGraphics array list an instance of the DrawingImage class for the shape of the active square
            staticShapesGraphics.add(
                    new DrawingImage(
                            activeSquare,
                            new Rectangle2D.Double(
                                    squareWidth * activePiece.getX(),
                                    squareWidth * activePiece.getY(),
                                    activeSquare.getWidth(null),
                                    activeSquare.getHeight(null)
                            )
                    )
            );
        }
        // Traverse the whitePieces array list with a for loop to get coordinates and load piece image
        for (int i = 0; i < whitePieces.size(); i++) {
            int COL = whitePieces.get(i).getX();
            int ROW = whitePieces.get(i).getY();
            Image piece = loadImage("src/main/resources/images/white_pieces/" + whitePieces.get(i).getFilePath());
            piecesGraphics.add(
                    new DrawingImage(
                            piece,
                            new Rectangle2D.Double(
                                    squareWidth * COL,
                                    squareWidth * ROW,
                                    piece.getWidth(null),
                                    piece.getHeight(null)
                            )
                    )
            );
        }
        // Traverse the whitePieces array list with a for loop to get coordinates and load piece image
        for (int i = 0; i < blackPieces.size(); i++) {
            int COL = blackPieces.get(i).getX();
            int ROW = blackPieces.get(i).getY();
            Image piece = loadImage("src/main/resources/images/black_pieces/" + blackPieces.get(i).getFilePath());
            piecesGraphics.add(
                    new DrawingImage(
                            piece,
                            new Rectangle2D.Double(
                                    squareWidth * COL,
                                    squareWidth * ROW,
                                    piece.getWidth(null),
                                    piece.getHeight(null)
                            )
                    )
            );
        }
        // This method tells Swing that an area of the window is "dirty" and then it repaints.
        this.repaint();
    }

    // Gets the right piece of the whitePieces array list according to the right coordinates
    public Piece getPiece(int x, int y) {
        for (Piece piece : whitePieces) {
            if (piece.getX() == x && piece.getY() == y) {
                return piece;
            }
        }
        // Gets the right piece of the blackPieces array list according to the right coordinates
        for (Piece piece : blackPieces) {
            if (piece.getX() == x && piece.getY() == y) {
                return piece;
            }
        }
        return null;
    }

    // MouseAdapter class receive mouse events
    private final MouseAdapter mouseAdapter = new MouseAdapter() {
        public void mousePressed(MouseEvent e) {
            // Gets the coordinates of the mouse
            int mouseX = e.getX();
            int mouseY = e.getY();

            // Defines clicked coordinates in the grid
            int clickedRow = mouseY / squareWidth;
            int clickedColumn = mouseX / squareWidth;

            // Decides the turn
            boolean isWhitesTurn = turnCounter % 2 != 1;

            // Gets the clicked piece at coordinates
            Piece clickedPiece = getPiece(clickedColumn, clickedRow);

            // Logic actions :
            if (activePiece == null // Not selected piece yet
                    && clickedPiece != null // True if there is a piece on the clicked square
                    && ((isWhitesTurn && clickedPiece.isWhite())    // Whites turn
                    || (!isWhitesTurn && clickedPiece.isBlack()))   // Blacks turn
            ) {
                // Select the active piece
                activePiece = clickedPiece;
            } else if (activePiece != null  // Selected piece
                    && activePiece.getX() == clickedColumn
                    && activePiece.getY() == clickedRow
            ) {
                // Unselect the piece
                activePiece = null;
            } else if (activePiece != null  // Selected piece
                    && activePiece.canMove(clickedColumn, clickedRow)   // True if the piece can move at the coordinates
                    && ((isWhitesTurn && activePiece.isWhite())     // Whites turn
                    || (!isWhitesTurn && activePiece.isBlack()))    // Black turn
            ) {
                // If a piece is there, we can kill it so we can be there
                if (clickedPiece != null) {
                    if (clickedPiece.isWhite()) {
                        whitePieces.remove(clickedPiece);   // Remove from array list
                    } else {
                        blackPieces.remove(clickedPiece);   // Remove from array list
                    }
                }
                // Sets new coordinates to the active piece : do move
                activePiece.setX(clickedColumn);
                activePiece.setY(clickedRow);

                // if piece is a pawn set has_moved to true
                if (activePiece.getClass().equals(Pawn.class)) {
                    Pawn castedPawn = (Pawn) (activePiece);
                    castedPawn.setHasMoved(true);
                }
                // Reset active piece
                activePiece = null;
                // The turn will change
                turnCounter++;
            }
            // Draw new state of the board (from arrays list and pieces coordinates)
            drawBoard();
        }
    };


    private void adjustShapePositions(double dx, double dy) {
        staticShapesGraphics.get(0).adjustPosition(dx, dy);
        this.repaint();
    }

    // Load an image from a string path
    private Image loadImage(String imageFile) {
        try {
            return ImageIO.read(new File(imageFile));
        } catch (IOException e) {
            return NULL_IMAGE;
        }
    }

    // This part of the code comes from many sources in stackOverflow.
    // It is basically necessary to render the graphics of the game
    @Override
    protected void paintComponent(Graphics game) {

        super.paintComponent(game);

        Graphics2D g2 = (Graphics2D) game;
        drawBackground(g2);
        drawShapes(g2);
    }

    private void drawBackground(Graphics2D g2) {
        g2.setColor(getBackground());
        g2.fillRect(0, 0, getWidth(), getHeight());
    }

    private void drawShapes(Graphics2D g2) {
        for (DrawingShape shape : staticShapesGraphics) {
            shape.draw(g2);
        }
        for (DrawingShape shape : piecesGraphics) {
            shape.draw(g2);
        }
    }
}


