package board;

import javax.swing.*;
import java.awt.*;

public class BoardFrame extends JFrame {

    Component component;

    // Constructor
    public BoardFrame() {

        // Window title
        this.setTitle("Chess Game for CLOO");
        // Window Icon
        ImageIcon icon = new ImageIcon("src/main/resources/images/IbexIcon.png");
        this.setIconImage(icon.getImage());
        // Window X button function
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        // Window : Sets whether this frame is resizable by the user.
        this.setResizable(false);
        // Creates a new component : the Board
        component = new Board();
        // Add the component to the frame
        this.add(component, BorderLayout.CENTER);
        // Window position
        this.setLocation(375, 50);
        // pack() : Causes this Window to be sized to fit the preferred size and layouts of its subcomponents.
        this.pack();
        // Window visibility
        this.setVisible(true);
    }
}
