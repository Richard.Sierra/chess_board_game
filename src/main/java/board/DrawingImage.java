package board;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class DrawingImage implements DrawingShape {

    public Image image;
    public Rectangle2D rectangle2D;

    public DrawingImage(Image image, Rectangle2D rectangle2D) {
        this.image = image;
        this.rectangle2D = rectangle2D;
    }

    @Override
    public boolean contains(Graphics2D g2, double x, double y) {
        return rectangle2D.contains(x, y);
    }

    @Override
    public void adjustPosition(double dx, double dy) {
        rectangle2D.setRect(rectangle2D.getX() + dx, rectangle2D.getY() + dy, rectangle2D.getWidth(), rectangle2D.getHeight());
    }

    @Override
    public void draw(Graphics2D g2) {
        Rectangle2D bounds = rectangle2D.getBounds2D();
        g2.drawImage(image, (int) bounds.getMinX(), (int) bounds.getMinY(), (int) bounds.getMaxX(), (int) bounds.getMaxY(),
                0, 0, image.getWidth(null), image.getHeight(null), null);
    }
}
