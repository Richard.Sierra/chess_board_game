import board.BoardFrame;

public class Game {

    public BoardFrame boardFrame;

    public static void main(String[] args) {

        Game game = new Game();
        game.boardFrame = new BoardFrame();
        game.boardFrame.setVisible(true);
    }
}
